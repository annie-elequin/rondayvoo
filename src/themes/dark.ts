const dark = {
  fonts: {
    body: 'system-ui, sans-serif',
    heading: '"Avenir Next", sans-serif',
    monospace: 'Menlo, monospace',
  },
  colors: {
    text: '#fff',
    background: '#222',
    mid: '#333',
    foreground: '#444',
    primary: '#33e',
  },
}

export default dark;