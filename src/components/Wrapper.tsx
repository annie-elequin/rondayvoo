/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'

export default function Wrapper({ children }) {
  return (
    <div
      sx={{
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        bg: 'background',
        color: 'text',
      }}
    >
      {children}
    </div>
  )
}
