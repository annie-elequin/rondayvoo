import React from 'react'

export default function Flex({ children, style = {}, ...props }) {
  return (
    <div style={{ display: 'flex', flex: 1, ...style }} {...props}>
      {children}
    </div>
  )
}
