/** @jsx jsx */
import { jsx } from 'theme-ui'
import React, { useContext } from 'react'
import { useRoutes } from 'hookrouter'
import { Store } from './AppContext'
import Home from './publicScenes/Home'
import Flex from './components/Flex'
import { ThemeProvider } from 'theme-ui'
import dark from './themes/dark'
import Login from './publicScenes/Login'

const publicRoutes = {
  '/': () => <Home />,
  '/login': () => <Login />,
}

const privateRoutes = {
  '/': () => <div style={{ width: 50, height: 50, backgroundColor: 'red' }} />,
}

function App() {
  const publicRouteResult = useRoutes(publicRoutes)
  const privateRouteResult = useRoutes(privateRoutes)
  const { user } = useContext(Store)
  return (
    <ThemeProvider theme={dark}>
      <Flex sx={{ backgroundColor: 'text', justifyContent: 'center', alignItems: 'stretch' }}>
        {user ? privateRouteResult : publicRouteResult}
      </Flex>
    </ThemeProvider>
  )
}

export default App
