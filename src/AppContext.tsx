import React from 'react'

const initialState = {
  user: null,
}

export const Store = React.createContext(initialState)

export default function AppContext({ children }) {
  return <Store.Provider value={{ user: null }}>{children}</Store.Provider>
}
