/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import Wrapper from '../components/Wrapper'
import { Card, CardHeader, CardTitle, CardImg, CardBody, CardFooter, Button } from 'shards-react'

export default function Login() {
  return (
    <Wrapper>
      <Card sx={{ bg: 'foreground' }}>
        <CardHeader>Card header</CardHeader>
        <CardBody>
          <CardTitle>Lorem Ipsum</CardTitle>
          <p>Lorem ipsum dolor sit amet.</p>
        </CardBody>
        <CardFooter>
          <Button>Read more &rarr;</Button>
        </CardFooter>
      </Card>
    </Wrapper>
  )
}
